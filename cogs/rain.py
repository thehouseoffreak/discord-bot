import math
import discord
from discord.ext import commands
from utils import rpc_module, mysql_module, checks, parsing
import random

rpc = rpc_module.Rpc()
mysql = mysql_module.Mysql()


class Rain:
    def __init__(self, bot):
        self.bot = bot
        soak_config = parsing.parse_json('config.json')['soak']
        config = parsing.parse_json('config.json')         
        '''
        Rain on specified role
        '''
        self.prefix = config["prefix"]           

    @commands.command(pass_context=True)
    async def rain(self, ctx, amount: float, role_id=""):
        """Tip all online users"""
        
        snowflake = ctx.message.author.id

        mysql.check_for_user(snowflake)
        balance = mysql.get_balance(snowflake, check_update=True)

        if float(balance) < amount:
            await self.bot.say("{} **:warning:You cannot rain more {} than you have!:warning:**".format(ctx.message.author.mention))
            return

        users_list=[]
        if role_id=="" or role_id=="@everyone" or role_id=="@here" or role_id=="all":
            for user in ctx.message.server.members:
                if mysql.check_for_user(user.id) is not None:
                    users_list.append(user)
        else:
            server_roles = ctx.message.server.roles
            role=[x for x in server_roles if x.mention==role_id]
            if len(role)==0:
                await self.bot.say("{}, This role does not exist! To tip a single user, use {}tip command instead.".format(ctx.message.author.mention,self.prefix))
                return
            else:
                for user in ctx.message.server.members:
                    if role[0] in user.roles:
                        if mysql.check_for_user(user.id) is not None:
                            users_list.append(user)    

        #if role_id!="":
            #online_users = [x for x in users_list]
        #else:
            #online_users = [x for x in users_list]
        online_users = [x for x in users_list]

        if ctx.message.author in online_users:
            online_users.remove(ctx.message.author)

        online_users=[x for x in online_users if x.bot==False]

        len_receivers = len(online_users)

        if len_receivers == 0:
            await self.bot.say("{}, you are all alone if you don't include bots! Trying raining when people are online.".format(ctx.message.author.mention))
            return

        amount_split = math.floor(float(amount) * 1e8 / len_receivers) / 1e8
        if amount_split == 0:
            await self.bot.say("{} **:warning:{:.8f} {} is not enough to split between {} users:warning:**".format(ctx.message.author.mention, float(amount), len_receivers))
            return

        if role_id=="all" or role_id=="@everyone" or role_id=="@here":
            await self.bot.say("{} **Sorry, Raining the {} role is not permitted.**".format(ctx.message.author.mention, role_id))
        else:
            receivers = []
            if role_id == "":
                for i in range(len_receivers):
                    user = online_users
                    receivers.append(user)
                    online_users.remove(user)
                    mysql.check_for_user(user.id)
                    mysql.add_tip(snowflake, user.id, amount_split)

                await self.bot.say(":cloud_rain: {} **Rained {:.8f} {} on {} users** (Total {:.8f} {}) :cloud_rain:".format(ctx.message.author.mention, float(amount_split), len_receivers, float(amount)))
                users_soaked_msg = []
                idx = 0
                for users in receivers:
                    users_soaked_msg.append(users.mention)
                    idx += 1
                    if (len(users_soaked_msg) >= 25) or (idx == int(len_receivers)):
                        await self.bot.say("{}".format(' '.join(users_soaked_msg)))
                        del users_soaked_msg[:]
                        users_soaked_msg = []
            else:
                for i in range(len_receivers):
                    user = random.choice(online_users)
                    receivers.append(user)
                    online_users.remove(user)
                    mysql.check_for_user(user.id)
                    mysql.add_tip(snowflake, user.id, amount_split)

                long_soak_msg = ":cloud_rain: {} **Rained {:.8f} {} on {} {} users** (Total {:.8f} {}) :cloud_rain:".format(ctx.message.author.mention, float(amount_split), len_receivers, role_id, float(amount))

                #bot response:
                await self.bot.say(long_soak_msg)

                #parse into 25 username sized message chunks
                users_soaked_msg = []
                idx = 0
                for users in receivers:
                    users_soaked_msg.append(users.mention)
                    idx += 1
                    if (len(users_soaked_msg) >= 25) or (idx == int(len_receivers)):
                        await self.bot.say("{}".format(' '.join(users_soaked_msg)))
                        del users_soaked_msg[:]
                        users_soaked_msg = []

def setup(bot):
    bot.add_cog(Rain(bot))