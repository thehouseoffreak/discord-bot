from discord.ext import commands
from utils import parsing


class Invite:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def invite(self, ctx):
        """
        Get the bot's invite link
        """
        
        await self.bot.say(":tada: https://discordapp.com/api/oauth2/authorize?client_id=477114120299085854&permissions=0&scope=bot".format(self.bot.user.id))


def setup(bot):
    bot.add_cog(Invite(bot))
