import discord, os
from discord.ext import commands
from utils import checks, output, parsing
from aiohttp import ClientSession
import urllib.request
import json

class Stats:
    def __init__(self, bot: discord.ext.commands.Bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def price(self, ctx, amount=1):
        """
        Shows Aevo price information
        """

        headers={"user-agent" : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36"}
        try:
            async with ClientSession() as session:
                async with session.get("https://coinlib.io/api/v1/coin?key=13d3539f084c2a0a&pref=BTC&symbol=AEVO", headers=headers) as response:
                    responseRaw = await response.read()
                    priceData = json.loads(responseRaw)
                    for item in priceData:
                        embed= discord.Embed(colour=0x00FF00)
                        embed.set_author(name='Aevo Information', icon_url="http://explorer.aevocoin.net/images/logo.png")
                        embed.add_field(name="Price (BTC)", value="${}".format(priceData['price']))
                        embed.add_field(name="24 Hour Volume ", value="${}".format(priceData['total_volume_24h']))  
                        embed.set_footer(text="https://aevocoin.net", icon_url="http://explorer.aevocoin.net/images/logo.png")
                    await self.bot.say(embed=embed)
        except:
            await self.bot.say(":warning: Error fetching prices!")


def setup(bot):
    bot.add_cog(Stats(bot))
