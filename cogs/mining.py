import discord, json, requests, math
from discord.ext import commands
from utils import rpc_module as rpc, parsing
from aiohttp import ClientSession


class Mining:
    def __init__(self, bot):
        self.bot = bot
        self.rpc = rpc.Rpc()

    @commands.command(pass_context=True)
    async def mining(self, ctx):
        """Show mining info"""

        mining_info = self.rpc.getmininginfo()
        height = mining_info["blocks"]
        difficulty = mining_info["difficulty"]/333333
        network_hs = mining_info["netmhashps"]
        network_Mhs = network_hs/1000000
        
        embed= discord.Embed(colour=0x00FF00)
        embed.set_author(name='Aevo Mining Information', icon_url="http://explorer.aevocoin.net/images/logo.png")
        embed.add_field(name="Current Height", value='{}'.format(height))
        embed.add_field(name="Network Difficulty", value='{0:.2f}'.format(difficulty))
        embed.add_field(name="Network Hashrate", value='{0:.2f} MH/s'.format(network_hs))
        await self.bot.say(embed=embed)

def setup(bot):
    bot.add_cog(Mining(bot))

